module gitlab.com/k1350/jwx_repository

go 1.20

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/lestrrat-go/jwx/v2 v2.0.9
	github.com/matoous/go-nanoid/v2 v2.0.0
	github.com/pkg/errors v0.9.1
)

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.1.0 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/lestrrat-go/blackmagic v1.0.1 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.4 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/option v1.0.1 // indirect
	golang.org/x/crypto v0.7.0 // indirect
)
