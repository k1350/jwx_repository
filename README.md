# 概要
JWK の生成、取得、削除を行う関数定義

## 想定テーブル構造

```sql
create table jwk (
    id char(21) primary key comment "キーID",
    jwk blob not null comment "JWK",
    expired_at datetime not null comment "有効期限"
) comment = "JWK";
```
