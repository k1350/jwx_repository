package jwx_repository

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/pkg/errors"
)

type AnyString struct{}

func (a AnyString) Match(v driver.Value) bool {
	_, ok := v.(string)
	return ok
}

type AnyBytes struct{}

func (a AnyBytes) Match(v driver.Value) bool {
	_, ok := v.([]byte)
	return ok
}

func TestCreate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewJWKRepository(db)
	s := regexp.QuoteMeta("INSERT INTO jwk (id, jwk, expired_at) VALUES (?, ?, ?)")

	now := time.Now().UTC()

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, AnyBytes{}, now.AddDate(0, 1, 14)).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := repo.Create(now)

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, AnyBytes{}, now.AddDate(0, 1, 14)).
			WillReturnError(fmt.Errorf("some error"))

		err := repo.Create(now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, DBError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestGetLatestKey(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewJWKRepository(db)
	s := regexp.QuoteMeta("SELECT id, jwk, expired_at FROM jwk WHERE expired_at >= ? ORDER BY expired_at DESC LIMIT 1")

	now := time.Now().UTC()

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "jwk", "expired_at"}).
			AddRow("test", []byte("test"), now)
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(now).
			WillReturnRows(rows)

		expected := &JWK{
			ID:        "test",
			JWK:       []byte("test"),
			ExpiredAt: now,
		}

		key, err := repo.GetLatestKey(now)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(key, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", key, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：キーなし", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(now).
			WillReturnError(sql.ErrNoRows)

		_, err := repo.GetLatestKey(now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：予期しないエラー", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(now).
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.GetLatestKey(now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, DBError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestGetValidKeySet(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewJWKRepository(db)
	s := regexp.QuoteMeta("SELECT id, jwk, expired_at FROM jwk WHERE expired_at >= ?")

	now := time.Now().UTC()

	t.Run("正常系", func(t *testing.T) {
		key1, _ := CreateKey()
		key2, _ := CreateKey()
		raw1, _ := json.MarshalIndent(key1, "", "  ")
		raw2, _ := json.MarshalIndent(key2, "", "  ")

		rows := sqlmock.NewRows([]string{"id", "jwk", "expired_at"}).
			AddRow(key1.KeyID(), raw1, now).
			AddRow(key2.KeyID(), raw2, now.AddDate(0, 0, 1))
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(now).
			WillReturnRows(rows)

		expected := jwk.NewSet()
		expected.AddKey(key1)
		expected.AddKey(key2)

		keys, err := repo.GetValidKeySet(now)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(keys, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", keys, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(now).
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.GetValidKeySet(now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, DBError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestDeleteOldKey(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewJWKRepository(db)
	s := regexp.QuoteMeta("DELETE FROM jwk WHERE expired_at < ?")

	now := time.Now().UTC()

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(now).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := repo.DeleteOldKey(now)

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(now).
			WillReturnError(fmt.Errorf("some error"))

		err := repo.DeleteOldKey(now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, DBError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
