package jwx_repository

import (
	"crypto/rand"
	"database/sql"
	"encoding/json"
	"github.com/pkg/errors"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/matoous/go-nanoid/v2"
)

var (
	// 対象リソースが存在しない
	NotFoundError = errors.New("Not Found")
	// DB関連のエラー
	DBError = errors.New("DB error")
	// JWK作成に失敗
	CreateJWKError = errors.New("Create JWK failed")
)

type JWK struct {
	ID        string
	JWK       []byte
	ExpiredAt time.Time
}

type JWKRepository struct {
	db *sql.DB
}

func NewJWKRepository(db *sql.DB) *JWKRepository {
	return &JWKRepository{db}
}

func (j *JWK) ConvertToJwkKey() (jwk.Key, error) {
	return jwk.ParseKey([]byte(j.JWK))
}

func (r *JWKRepository) Create(now time.Time) error {
	key, err := CreateKey()
	if err != nil {
		return err
	}
	raw, err := json.MarshalIndent(key, "", "  ")
	if err != nil {
		return errors.Wrap(CreateJWKError, err.Error())
	}

	s := "INSERT INTO jwk (id, jwk, expired_at) VALUES (?, ?, ?)"
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return errors.Wrap(DBError, err.Error())
	}
	defer stmt.Close()

	// 有効期間一か月半
	_, err = stmt.Exec(key.KeyID(), raw, now.AddDate(0, 1, 14))
	if err != nil {
		return errors.Wrap(DBError, err.Error())
	}

	return nil
}

func (r *JWKRepository) GetLatestKey(now time.Time) (*JWK, error) {
	s := "SELECT id, jwk, expired_at FROM jwk WHERE expired_at >= ? ORDER BY expired_at DESC LIMIT 1"
	j := &JWK{}
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(DBError, err.Error())
	}
	defer stmt.Close()
	err = stmt.QueryRow(now).Scan(&j.ID, &j.JWK, &j.ExpiredAt)
	switch {
	case err == sql.ErrNoRows:
		return nil, NotFoundError
	case err != nil:
		return nil, errors.Wrap(DBError, err.Error())
	}
	return j, nil
}

func (r *JWKRepository) GetValidKeySet(now time.Time) (jwk.Set, error) {
	s := "SELECT id, jwk, expired_at FROM jwk WHERE expired_at >= ?"
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(DBError, err.Error())
	}
	defer stmt.Close()
	rows, err := stmt.Query(now)
	if err != nil {
		return nil, errors.Wrap(DBError, err.Error())
	}
	defer rows.Close()

	privset := jwk.NewSet()
	for rows.Next() {
		j := &JWK{}
		err = rows.Scan(&j.ID, &j.JWK, &j.ExpiredAt)
		if err != nil {
			return nil, errors.Wrap(DBError, err.Error())
		}

		key, err := j.ConvertToJwkKey()
		if err != nil {
			return nil, errors.Wrap(DBError, err.Error())
		}

		privset.AddKey(key)
	}

	err = rows.Err()
	if err != nil {
		return nil, errors.Wrap(DBError, err.Error())
	}

	return privset, nil
}

func (r *JWKRepository) DeleteOldKey(now time.Time) error {
	stmt, err := r.db.Prepare("DELETE FROM jwk WHERE expired_at < ?")
	if err != nil {
		return errors.Wrap(DBError, err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(now)
	if err != nil {
		return errors.Wrap(DBError, err.Error())
	}

	return nil
}

func CreateKey() (jwk.Key, error) {
	raw := make([]byte, 256)
	_, err := rand.Read(raw)
	if err != nil {
		return nil, errors.Wrap(CreateJWKError, err.Error())
	}
	key, err := jwk.FromRaw(raw)
	if err != nil {
		return nil, errors.Wrap(CreateJWKError, err.Error())
	}
	if _, ok := key.(jwk.SymmetricKey); !ok {
		return nil, CreateJWKError
	}
	id, err := gonanoid.New(21)
	if err != nil {
		return nil, errors.Wrap(CreateJWKError, err.Error())
	}

	key.Set(jwk.KeyIDKey, id)
	key.Set(jwk.AlgorithmKey, jwa.HS256)

	return key, nil
}
